﻿// file:            getServiceDefinitions.ashx.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!


using System;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Web.Caching;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.Collections.Specialized;
using Madgic.ServiceDefinitions;
using Madgic.ServiceDefinitions.Json;

namespace Madgic.ServiceDefintions.handlers
{
    /// <summary>
    /// Summary description
    /// </summary>
    public class GetServiceDefinitions : IHttpHandler
    {
        const string PROD_HOST = "mercator.trentu.ca";
        const string DEV_HOST = "albers.trentu.ca";
        const string JSON = "f=json";
        const string INFO = "info/iteminfo";
        const string REST_PATH = "/arcgis/rest/services";
        //
        protected HttpResponse resp = null;
        //
        public void ProcessRequest(HttpContext context)
        {
            string server_path = "";
            Uri uri = context.Request.Url;
            // check whether local, dev or production
            if (uri.Host == "localhost")
            {
                server_path = uri.Scheme + "://" + DEV_HOST + REST_PATH;
            }
            else
            {
                server_path = uri.Scheme + "://" + ((uri.Host == "madgic.trentu.ca" || uri.Host == "mercator.trentu.ca") ? PROD_HOST : DEV_HOST) + REST_PATH;
            }
            HttpResponse response = context.Response; // set up the response
            JavaScriptSerializer serializer = new JavaScriptSerializer(); // create a serializer object - also handy for deserializing
            NameValueCollection urlParams = context.Request.QueryString; // create a collection from the query params
            // remember the folder param, please, we need it to query the whole project folder
            string folder = urlParams.Get("dir");
            string collectionPath = server_path + "/" + folder;
            // setup the initial request to the GIS server for a list of all its services in the provided sub-folder
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(collectionPath + "?f=json");
            req.Method = context.Request.HttpMethod;
            req.ServicePoint.Expect100Continue = false;
            req.Referer = context.Request.Headers["referer"]; // note the header!
            // get the repsionse from req - which is looking for all services in the collection
            WebResponse serverResponse = null;
            try
            {
                serverResponse = req.GetResponse(); //serverResponse is the response from AGS
                if (serverResponse != null)
                {
                    
                    using (Stream byteStream = serverResponse.GetResponseStream())
                    {
                        response.ContentType = "application/json";
                        using (StreamReader sr = new StreamReader(byteStream))
                        {

                            string strResponse = sr.ReadToEnd(); // read the byte stream into a string - vroom, vroom!                          
                            Services services = serializer.Deserialize<Services>(strResponse); // fill the <Services>services object with from deserialized text
                            List<MadgicItemInfo> infoItems = new List<MadgicItemInfo>();
                            // oh, we'll need a queue of stuff to do as well. set this up and iterate each into the queue.
                            var taskQueue = new Queue<Task>();
                            foreach (ServiceAccess sa in services.services)
                            {
                                taskQueue.Enqueue(Task.Factory.StartNew(() =>
                                {
                                    var client = new WebClient(); // do work with a client
                                    string path = server_path + "/" + sa.name + "/" + sa.type + "/" + INFO + "?" + JSON;  // the path we'll need to get the ItemInfo
                                    string json = client.DownloadString(path); // get the full service spec as json using the web client (above)
                                    MadgicItemInfo itemInfo = serializer.Deserialize<MadgicItemInfo>(json);
                                    itemInfo.postProcess(server_path + "/" + sa.name + "/" + sa.type);
                                    infoItems.Add(itemInfo);  // add to the srevice list

                                }));
                            }
                            // fire the queue to start making requests  - when finished, pack the results into
                            // a menu json object and serialize before returning it to the http client (browser)
                            Task.Factory.ContinueWhenAll(taskQueue.ToArray(), completedTasks =>
                            {
                                // once everything is finished requesting (ContinueWhenAll()), build a menu and return
                                Menu menu = new Menu(infoItems);
                                JsonResponse jsonResponse = new JsonResponse(ref menu);
                                response.Write(jsonResponse.Json);

                            }).Wait();
                        }
                        serverResponse.Close();
                    }
                }
                //response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (WebException ex)
            {
                response.ContentType = "application/json";
                response.StatusCode = 500;
                response.StatusDescription = ex.Status.ToString();
                var err = new
                {
                    responseType = "error",
                    errorCode = response.StatusCode,
                    errorType = response.StatusDescription,
                    errorMessage = ex.Message,
                    errorDescription = ""
                };
                response.Write(serializer.Serialize(err));
                //response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                return;
            }
            catch (Exception ex)
            {
                response.ContentType = "application/json";
                response.StatusCode = 500;
                response.StatusDescription = "Unclassified Exception";
                var err = new
                {
                    responseType = "error",
                    errorCode = response.StatusCode,
                    errorType = response.StatusDescription,
                    errorMessage = ex.Message,
                    errorDescription = ""
                };
                response.Write(serializer.Serialize(err));
                //response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                return;
            }
            finally
            {
                serverResponse.Dispose();
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}