﻿var App;
(function ($) {
    App = function () {
        var _config = {
            url: "getServiceDefinitions.ashx"
        }
        this.init = function () {
            var req = new this.Request({
                f: "json",
                dir: "reha"
            });
        }
        this.render = function (domNode, data) {
            if (data.menu.length > 0) {
                var root = $("<div>", {
                    "html": "Services",
                    "class": "root"
                });
                $.each(data.menu, function (n, collection) {

                    var container = $("<div>")
                        .addClass("container")
                        .appendTo(root);

                    var clickable = $("<div>")
                        .click(function () {
                            if ($(this).siblings(".services").css("display") == "none") {
                                $(this).siblings(".services").css("display", "block");
                                $(this).find(".collapser").html("-");
                            } else {
                                $(this).siblings(".services").css("display", "none");
                                $(this).find(".collapser").html("+");
                            }
                        })
                        .addClass("collection")
                        .appendTo(container);

                    var collapser = $("<span>", {
                        html: "-"
                    })
                        .addClass("collapser")
                        .appendTo(clickable);

                    var title = $("<span>", {
                        html: collection["name"]
                    }).appendTo(clickable);

                    var items = $("<div>")
                        .addClass("services")
                        .appendTo(container);

                    $.each(collection.items, function (i, o) {
                        $("<div>", {
                            html: o["title"]
                        })
                            .addClass("service")
                            .appendTo(items);
                    });
                });
                domNode.append(root);
            } else {
                domNode.append($("<div>", {
                    html: "Nothing here!"
                }))
            }
        }
        this.Request = function (_params) {
            this._request = $.getJSON(_config.url + "?" + $.param(_params));
            this._request.done(this._onSuccess);
        }
        this.Request.prototype.cancel = function () {
            this._request.abort();
        }
        this.Request.prototype._onSuccess = $.proxy(function (resp) {
                this.render($("body"), resp);
        }, this);
        this.Request.prototype._onNoResult = function () {

        }
        this.Request.prototype._onError = function (err) {
            console.log(err);
            $("body").html(err);
        }
        this.Request.prototype._always = function () {
            //TODO
        }
    }
})(window["jQuery"]);