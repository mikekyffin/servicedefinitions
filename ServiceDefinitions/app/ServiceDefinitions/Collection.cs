﻿// file:            Collection.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Madgic.ServiceDefinitions
{
    public class Collection
    {
        private string _name;
        public List<MenuItem> items; // we'll call Add elsewhere
        // we certainly need a name or we can't really have a collection - can we?
        public Collection(string collectionName)
        {
            items = new List<MenuItem>().OrderBy(mi => mi.title).ToList();
            // we could do some validation here on the name(s), if necessary
            _name = collectionName;
        }
        // 
        public string name
        {
            get 
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
    }
}