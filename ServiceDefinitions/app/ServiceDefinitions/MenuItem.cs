﻿// file:            MenuItem.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Web;

namespace Madgic.ServiceDefinitions
{
    public class MenuItem
    {
        // public members for serialization
        public string collection { get; set;}
        public string url { get; set;}
        public string type { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public string credits { get; set; }
        public string description { get; set; }
        public string summary { get; set; }
        public string category { get; set; }
        public List<String> keywords { get; set; }
        public List<String> supportedLayers { get; set; }
        // constructor with title as arg
        public MenuItem(string inTitle) {
            this.title = inTitle;
        } 
    }
}