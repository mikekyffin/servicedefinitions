﻿// file:            Menu.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;

namespace Madgic.ServiceDefinitions
{
    public class Menu
    {
        // the only member to be serialized! 
        // this will be the menu JSON returned to the client.
        public List<Collection> menu = new List<Collection>();
        // constructor
        public Menu() { 
        
        }
        // constructor
        public Menu(List<MadgicItemInfo> list)
        {
            menu = build(filter(list)).OrderBy(c => c.name).ToList();           
        }

        // filter out anything that doesn't have a collection.
        // note the use of an anonymous method delegate.
        private List<MadgicItemInfo> filter(List<MadgicItemInfo> list)
        {
            List<MadgicItemInfo> lc = list.FindAll(delegate(MadgicItemInfo mii)
            {
                return mii.extendedInfo.collection != null;
            });
            return lc;
        }

        // build the menu list to be serialized
        private List<Collection> build(List<MadgicItemInfo> list)
        {
            // this will be returned from the method
            List<Collection> menuHeaders = new List<Collection>();
            // go through the supplied list and build an list object that has
            // headers and subheaders. each header holds n number of items.
            foreach (MadgicItemInfo mii in list)
            {
                if (menuHeaders.Where(i => i.name == mii.extendedInfo.collection).FirstOrDefault() != null)
                {
                    // oh, we already have that MenuHeader? just create a new item and add it to that one
                    MenuItem menuItem = new MenuItem(mii.title);
                    // complete the fields
                    completeFields(mii, ref menuItem);
                    menuHeaders.Where(i => i.name == mii.extendedInfo.collection).FirstOrDefault().items.Add(menuItem);
                }
                else
                {
                    //oh, we need as new one?
                    // create a new collection
                    Collection collection = new Collection(mii.extendedInfo.collection);
                    // create a new item
                    MenuItem menuItem = new MenuItem(mii.title);
                    // complete the fields
                    completeFields(mii, ref menuItem);
                    // add item to the new collection
                    collection.items.Add(menuItem);
                    // add the collection to the list of collections
                    menuHeaders.Add(collection);
                }
            }
            return menuHeaders;
        }

        // TODO: I think I hate this
        // note "ref" below. see <http://msdn.microsoft.com/en-us/library/14akc2c7%28v=vs.71%29.aspx> for
        // what this is.
        public static void completeFields(MadgicItemInfo mii, ref MenuItem mi)
        {
            mi.collection = mii.extendedInfo.collection;
            mi.url = mii.url;
            mi.type = mii.type;
            mi.name = mii.name;
            mi.credits = mii.accessInformation;
            mi.description = mii.description;
            mi.summary = mii.summary;
            mi.category = "";
            mi.keywords = mii.extendedInfo.keywords;
            mi.supportedLayers = mii.extendedInfo.supportedTypes;
        }

    }
}