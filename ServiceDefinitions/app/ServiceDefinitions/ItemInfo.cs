﻿// file:            ItemInfo.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Madgic.ServiceDefinitions
{
    /// <summary>
    /// simple obj for de/serialization from and to json - matches AGS server iteminfo.
    /// make C# classes quickly from AGS REST endpoints by using http://json2csharp.com/. the converter is really useful.
    /// </summary>

    // see example: http://<server>/arcgis/rest/services/reha/soils/MapServer/info/iteminfo
    public class ItemInfo
    {
            public string culture { get; set; }
            public string name { get; set; }
            // assigned by GIS server
            public string guid { get; set; }
            public string catalogPath { get; set; }
            public string snippet { get; set; }
            public string description { get; set; }
            public string summary { get; set; }
            public string title { get; set; }
            public List<string> tags { get; set; }
            public string type { get; set; }
            public List<string> typeKeywords { get; set; }
            public string thumbnail { get; set; }
            public string url { get; set; }
            public List<List<double>> extent { get; set; }
            public string spatialReference { get; set; }
            public string accessInformation { get; set; }
            public string licenseInfo { get; set; }
    }
}