﻿// file:            Services.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;

namespace Madgic.ServiceDefinitions
{
    
    // a class to contain the base view of an AGS sub-folder
    public class Services
    {
        // name
        public string name { get; set; }        
        // type - MapSever, ImageServer, etc.
        public string type { get; set; }
        // 10.0, 10.1, etc.
        public double currentVersion { get; set; }       
        // always blank at the sub-folder level? 
        public List<object> folders { get; set; }       
        // a list of off the services and their type that are available for access
        public List<ServiceAccess> services { get; set; }
        
    }
    
}