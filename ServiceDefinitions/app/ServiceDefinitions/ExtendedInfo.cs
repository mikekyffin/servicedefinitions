﻿// file:            ExtendedInfo.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;

namespace Madgic.ServiceDefinitions
{
    /// <summary>
    /// more properties to be added to ItemInfo re. de/serialization from and to json.
    /// see: ItemInfo.cs
    /// </summary>
    public class ExtendedInfo
    {
        // collection name
        public string collection { get; set; }       
        // list of supported layer types that are supported
        // by this service
        public List<string> supportedTypes { get; set; }    
        // list of keywords for searching!
        public List<string> keywords { get; set; }           
        // can the service support an image service layer?
        public Boolean image { get; set; }           
        // can the service support a dynamic service layer?
        public Boolean dynamic { get; set; }            
        // can the service support a kml service layer? is the kml 
        // capability enabled and export of kml possible?
        public Boolean kml { get; set; }            
        // can the service support a tiled service layer - does it 
        // have tiles associated?
        public Boolean tiled { get; set; }            
        // can the service support a WMS?
        public Boolean wms { get; set; }            
        // constructor
        public ExtendedInfo() { }
    }
}