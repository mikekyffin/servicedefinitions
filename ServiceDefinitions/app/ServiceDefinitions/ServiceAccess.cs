﻿// file:            ServiceAccess.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Madgic.ServiceDefinitions
{
    // this is the base info about a service and contains nothing but
    // a name and a type (MapServer, ImageServer, etc.)
    // the name using the foloowing format "<sub-folder>/<service-name>"
    // example: "reha/nts78"
    public class ServiceAccess
    {
        public string name { get; set; }
        public string type { get; set; }
    }

}