﻿// file:            MadgicItemInfo.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Madgic.ServiceDefinitions
{
    /// <summary>
    /// extend ItemInfo using ExtendedItemInfo re. de/serialization from and to json.
    /// see: ItemInfo.cs, ExtendedInfo.cs
    /// </summary>
    public class MadgicItemInfo : ItemInfo
    {
        // an enum of the possible service types
        enum ServiceTypes { dynamic, tiled, image, wms, kml };
        // more info for the client to build UI/UX from - see ExtendedInfo.cs
        private ExtendedInfo _extendedInfo;       
        // class constructor
        public MadgicItemInfo() {
            _extendedInfo = new ExtendedInfo();   
        }

        // process the base class to prepare extendedInfo (see class) for serialization
        // and return to the client
        public void postProcess(string url) {
            base.url = url;

            if (base.tags.Count > 0 && !string.IsNullOrEmpty(base.tags[0]))
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();      
                foreach (string tag in base.tags)
                {
                    string trimmed = tag.Trim(); // trim whitespace if neeeded            
                    // fill a dictionary with key.value pair so we don't need to keep track of the
                    // order in which people fill out the tags. important.
                    string[] kv = trimmed.Split(new Char[] { ':' });
                    dict.Add(kv[0], kv[1]);
                    // assign collection to extendedInfo
                    if (dict.ContainsKey("collection"))
                    {
                        extendedInfo.collection = dict["collection"];
                    }
                    // assign types to extend info
                    if (dict.ContainsKey("types"))
                    {
                        if (dict["types"].IndexOf("|") > -1)
                        {
                            extendedInfo.supportedTypes = dict["types"].Split(new Char[] { '|' }).ToList<string>();
                        }
                        else
                        {
                            extendedInfo.supportedTypes = new List<string>();
                            extendedInfo.supportedTypes.Add(dict["types"]);
                        }
                    }
                    // assign keywords to extend info
                    if (dict.ContainsKey("keywords"))
                    {
                        if (dict["keywords"].IndexOf("|") > -1)
                        {
                            extendedInfo.keywords = dict["keywords"].Split(new Char[] { '|' }).ToList<string>();
                        }
                        else
                        {
                            extendedInfo.keywords = new List<string>();
                            extendedInfo.keywords.Add(dict["keywords"]);
                        }
                    }
                    // we have an enum for types of possible services. go through these and find 
                    // find out which ones are isted in the suppurtedTypes List.
                    // when one if found, set the appropriate Boolean member to true by reflection.
                    // we're doing to facilitate simpler queries on the client side to determine
                    // which kind of service(s) can be built via JavaSript.
                    if (extendedInfo.supportedTypes != null && extendedInfo.supportedTypes.Count > 0)
                    {
                        foreach (string st in Enum.GetNames(typeof(ServiceTypes)))
                        {
                            if (extendedInfo.supportedTypes.Contains(st))
                            {
                                PropertyInfo propertyInfo = extendedInfo.GetType().GetProperty(st);
                                propertyInfo.SetValue(extendedInfo, true);
                            }
                        }
                    }
                }
            }
        } 
        
        // public getter and setter for _extendedInfo property
        public ExtendedInfo extendedInfo
        {
            get
            {
                return _extendedInfo;
            }
            set
            {
                _extendedInfo = value;
            }
        }        
    }
}