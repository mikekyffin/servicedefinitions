﻿// file:            MenuItem.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Web.Script.Serialization;

namespace Madgic.ServiceDefinitions.Json
{
    public class JsonResponse
    {
        // this will be the ultimate response
        private string json;
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        public JsonResponse(ref Menu menu) {
            json = this.serializer.Serialize(menu);
        }
        // get the json that has been serialized and return
        public string Json {
            get {
                return json;
            }
        }

    }
}