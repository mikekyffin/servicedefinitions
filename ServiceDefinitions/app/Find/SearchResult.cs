﻿// file:            SearchResult.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Madgic.ServiceDefinitions;
using System.Reflection;


namespace Madgic.Find
{
    public class SearchResult
    {
        private int _score = 0;
        private String _name;
        private String _title;
        private MadgicItemInfo _mii;
        private MenuItem _mi;
        private List<string> _matchOn = new List<string>();

        // this is ludicrously basic. the idea is that eventually we can seach the associated
        // metadata as well - and scores can be assigned if search terms are found in the abstract
        // or longer description *as well* as the keywords. we would implement some regex searching
        // of that text to accomplish this.
        // in future, this will all probably be discarded in terms of a full-blown metadata search capability
        // a la http://geonetwork-opensource.org/
        // step 1. this
        // step 2. make this better
        // step 3. metadata server
        public SearchResult(MadgicItemInfo info, string searchText)
        {
            _mii = info;
            _mi = new MenuItem(_mii.title);
            Menu.completeFields(_mii, ref _mi);
            _name = info.name;
            _title = info.title;
            // call private seach methods by relection
            MethodInfo[] methodInfo = this.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            foreach (MethodInfo method in methodInfo) {
                method.Invoke(this, new Object[] { searchText });           
            }
        }

        private void searchTitle(string text)
        {
            if (_mii.title.ToUpper().Contains(text.ToUpper()))
            {
                _score += 9;
                _matchOn.Add("title");
            }
        }

        private void searchKeywords(string text)
        {
            List<String> caps = _mii.extendedInfo.keywords.Select(x => { return x.ToUpper(); }).ToList();
            if (_mii.extendedInfo.keywords != null && caps.Contains(text.ToUpper()))
            {
                _score += 6;
                _matchOn.Add("keyword");
            }
        }

        private void searchDescription(string text)
        {
            if (_mii.description.ToUpper().Contains(text.ToUpper()))
            {
                _score += 3;
                _matchOn.Add("description");
            }
        }

        private void searchSummary(string text)
        {
            if (_mii.summary.ToUpper().Contains(text.ToUpper()))
            {
                _score += 3;
                _matchOn.Add("summary");
            }
        }

        // public fields
        public int score
        {
            get
            {
                return _score;
            }
        }

        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public List<string> matchOn
        {
            get
            {
                return _matchOn;
            }
            set
            {
                _matchOn = value;
            }
        }

        public MadgicItemInfo itemInfo
        {
            get
            {
                return _mii;
            }
            set
            {
                _mii = value;
            }
        }

        public MenuItem menuItem
        {
            get
            {
                return _mi;
            }
        }
    }
}