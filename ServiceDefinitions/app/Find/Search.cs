﻿// file:            Search.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using Madgic.ServiceDefinitions;

namespace Madgic.Find
{
    public class Search {

        // to hold the response
        private SearchResponse _response; 
        // constructor
        public Search(List<MadgicItemInfo> infoItems, string text) {
            // set-up the search response
            this._response = new SearchResponse();
            foreach (MadgicItemInfo mii in infoItems) {
                SearchResult searchResult = new SearchResult(mii, text);
                if (searchResult.score > 0)
                {
                    this._response.add(searchResult);
                }
            }
        }

        // filter for keywords-only, if needed
        private List<MadgicItemInfo> filter(List<MadgicItemInfo> infoItems)
        {
            return infoItems.FindAll(delegate(MadgicItemInfo mii)
            {
                return mii.extendedInfo.keywords != null;
            });
        }

        // return the search repsonse (for serialization?)
        public SearchResponse response         
        {
            get
            {
                return _response;
            }
        }
    }        
}