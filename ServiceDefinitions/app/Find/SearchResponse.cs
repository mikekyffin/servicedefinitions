﻿// file:            SearchResponse.cs
// author(s):       mike kyffin
// build target:    ServiceDefinitions.dll
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// now featuring super-verbose commenting for internal use!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Madgic.Find
{
    public class SearchResponse
    {
        // list of search results. see SearchResult.cs
        private List<SearchResult> _results;
        // constructor
        public SearchResponse() { 
            _results = new List<SearchResult>();
        }
        // add a result
        public void add(SearchResult sr) {
            _results.Add(sr);
        }
        // to be serialized
        public List<SearchResult> results 
        {
            get
            {
                return _results.OrderByDescending(item => item.score).ToList();
            }
        }
    }
}